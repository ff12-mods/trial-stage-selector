// File size: 59520 Bytes
// Author:    t-hirose
// Source:    tri_a01.src
// Date:      00/00 00:00
// Binary:    section_000.bin

option authorName = "t-hirose/ffgriever";
option fileName = "tri_a01-selector.src";
option dataFile = "tri_a01.src.data";
option spawnOrder = {-1, 2, 1, -1, -1, -1, -1, -1};
option positionFlags = 0;
option unknownFlags1 = 0;
option unknownScale = {100, 0, 0};
option unknownPosition2d = {176, 768};

//======================================================================
//                Global and scratchpad variable imports                
//======================================================================
import global   short   global_flag[256] = 0x0;
import global   u_short global_var_1 = 0xcf4;
import global   int     mp_map_flg = 0x910;
import global   u_char  global_var_3 = 0x9df;
import global   u_char  global_var_4 = 0x9e0;
import global   u_char  global_var_5 = 0x9e1;
import global   u_char  global_var_6 = 0x9e2;
import global   u_char  global_var_7 = 0x9e3;
import global   u_char  global_var_8 = 0x9e4;
import scratch1 short   scratch1_var_a = 0x6;
import scratch1 short   scratch1_var_b = 0xc;



script setup(0)
{

	function init()
	{
		return;
	}


	function battle()
	{
		btlAtelSetupActorStart();
		btlAtelSetEntryStruct(0x3000044);
		return;
	}


	function event()
	{
		if (nowmap() == 0x483)
		{
			stopenvsoundall();
			setcharseplayall(0);
			musicfadeout(-1, 180);
			musicfadeoutsync();
			musicstop(-1);
			wait(1);
			musicclear(-1);
			if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicread(20);
			}
			else
			{
				musicread(91);
			}
		}
		if (nowmap() == 0x493)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x493)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(35);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x493 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x4a3)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x4a3)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(45);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x4a3 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x4b3)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x4b3)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(15);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x4b3 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x4c3)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x4c3)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(85);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x4c3 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x4d3)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x4d3)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(60);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x4d3 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x4e3)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x4e3)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(88);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x4e3 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x4f3)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x4f3)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(48);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x4f3 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x503)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x503)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(75);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x503 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		if (nowmap() == 0x513)
		{
			stopenvsoundall();
			setcharseplayall(0);
			if (global_var_1 >= 0x513)
			{
				musicfadeout(-1, 180);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicread(57);
				}
				else
				{
					musicread(91);
				}
			}
			if ((global_var_1 < 0x513 && ((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicplay(86);
			}
		}
		return;
	}
}


script btl_trap_ctrl(0)
{

	function init()
	{
		modelread(0x3000001);
		modelreadsync(0x3000001);
		settrapresource(0x3000001);
		return;
	}
}


script Map_Trial_Common_Process(0)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_c;             // pos: 0x0;

	function init()
	{
		local_var_c = 0;
		switch (nowmap())
		{
			case 0x4c4:
				local_var_c = 1;
				mp_map_flg = 0;
				break;
			case 0x49c:
			case 0x4d7:
				local_var_c = 1;
				mp_map_flg = 1;
				break;
			case 0x506:
				local_var_c = 1;
				mp_map_flg = 2;
				break;
			case 0x4ed:
				local_var_c = 1;
				mp_map_flg = 5;
				break;
			case 0x513:
				local_var_c = 1;
				mp_map_flg = 4;
				break;
			case 0x503:
				local_var_c = 1;
				mp_map_flg = 3;
				break;
			case 0x51e:
				break;
			default:
				clear_field_effect(mp_map_flg);
				break;
		}
		if (local_var_c == 1)
		{
			set_field_effect(mp_map_flg);
		}
		for (regI0 = 1; regI0 < 16; regI0 = (regI0 + 1))
		{
			setmapidmj(regI0, 0, 0);
			setmapidmj(regI0, 7, 0);
			setmapidmj(regI0, 1, 0);
			setmapidmj(regI0, 2, 0);
			setmapidmj(regI0, 3, 0);
			setmapidmj(regI0, 9, 0);
		}
		switch (nowmap())
		{
			case 0x49f:
			case 0x4e7:
			case 0x4eb:
			case 0x4ec:
			case 0x4ed:
				showmapmodel(2);
				hidemapmodel(1);
				setmapidfloor(1, 0, 0);
				setmapidfloor(1, 7, 0);
				setmapidfloor(1, 1, 0);
				setmapidfloor(1, 2, 0);
				setmapidfloor(1, 3, 1);
				setmapidfloor(1, 9, 0);
				setmapidwall(1, 5, 0);
				setmapidwall(1, 6, 0);
				break;
			case 0x4b5:
			case 0x4b6:
			case 0x4d4:
			case 0x4d5:
				setmapidfloor(1, 0, 0);
				setmapidfloor(1, 7, 0);
				setmapidfloor(1, 1, 0);
				setmapidfloor(1, 2, 0);
				setmapidfloor(1, 3, 1);
				setmapidfloor(1, 9, 0);
				setmapidwall(1, 5, 0);
				setmapidwall(1, 6, 0);
				break;
			case 0x4bd:
			case 0x4be:
				setmapidfloor(1, 0, 0);
				setmapidfloor(1, 7, 0);
				setmapidfloor(1, 1, 0);
				setmapidfloor(1, 2, 0);
				setmapidfloor(1, 3, 0);
				setmapidfloor(1, 4, 0);
				setmapidfloor(1, 9, 0);
				setmapidwall(1, 5, 0);
				setmapidwall(1, 6, 0);
				setmapidfloor(2, 0, 0);
				setmapidfloor(2, 7, 0);
				setmapidfloor(2, 1, 0);
				setmapidfloor(2, 2, 0);
				setmapidfloor(2, 3, 0);
				setmapidfloor(2, 4, 0);
				setmapidfloor(2, 9, 0);
				setmapidwall(2, 5, 0);
				setmapidwall(2, 6, 0);
				break;
			case 0x4c5:
			case 0x507:
			case 0x50b:
				bganimeplay_9e(0, 0, 0);
				bganimeplay_9e(1, 0, 0);
				setmapidwall(4, 5, 1);
				setmapidwall(4, 6, 1);
				setmapidfloor(2, 0, 0);
				setmapidfloor(2, 7, 0);
				setmapidfloor(2, 1, 0);
				setmapidfloor(2, 2, 0);
				setmapidfloor(2, 3, 0);
				bganimeplay_9e(3, 0, 0);
				bganimeplay_9e(4, 0, 0);
				bganimeplay_9e(5, 0, 0);
				bganimeplay_9e(6, 0, 0);
				bganimeplay_9e(7, 0, 0);
				bganimeplay_9e(8, 0, 0);
				bganimeplay_9e(2, 150, 150);
				setmapidfloor(1, 0, 0);
				setmapidfloor(1, 7, 0);
				setmapidfloor(1, 1, 0);
				setmapidfloor(1, 2, 0);
				setmapidfloor(1, 3, 0);
				setmapidfloor(1, 4, 0);
				setmapidfloor(1, 9, 0);
				setmapidwall(1, 5, 0);
				setmapidwall(1, 6, 0);
				break;
			case 0x4d3:
			case 0x4e3:
			case 0x506:
			case 0x4e5:
			case 0x4e6:
				bganimeplay_9e(0, 0x190, 0x190);
				bganimeplay_9e(1, 0x190, 0x190);
				break;
		}
		return;
	}
}


script トライアル監督(0)
{

	function init()
	{
		if (nowmap() == 0x483)
		{
			fadecancel_15a(2);
			fadeout(0);
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			suspendbattle();
		}
		if (!(unkCall_5be()))
		{
			if (((((((((nowmap() == 0x493 || nowmap() == 0x4a3) || nowmap() == 0x4b3) || nowmap() == 0x4c3) || nowmap() == 0x4d3) || nowmap() == 0x4e3) || nowmap() == 0x4f3) || nowmap() == 0x503) || nowmap() == 0x513))
			{
				fadecancel_15a(2);
				fadeout(0);
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				suspendbattle();
			}
		}
		registshape(0x3000100);
		unkCall_59d(0);
		shapewin(3, 0, 0, 0x780, 0x438, 0);
		setshape(3, 0, 0, 0);
		setshape(3, 4, 0, 1);
		setshapeshowstatus(3, 0, 0);
		setshapeshowstatus(3, 1, 0);
		setshapeshowstatus(3, 2, 0);
		setshapeshowstatus(3, 3, 0);
		setshapeshowstatus(3, 4, 0);
		setshapepos_183(3, 0, 0x63a, 0x170, 0);
		setshapepos_183(3, 4, 0x63a, 0x170, 0);
		if (((((((((((((((((((((((((nowmap() == 0x48f || nowmap() == 0x495) || nowmap() == 0x497) || nowmap() == 0x49b) || nowmap() == 0x49d) || nowmap() == 0x49e) || nowmap() == 0x4a3) || nowmap() == 0x4ae) || nowmap() == 0x4b4) || nowmap() == 0x4b7) || nowmap() == 0x4be) || nowmap() == 0x4c4) || nowmap() == 0x4ce) || nowmap() == 0x4cf) || nowmap() == 0x4ed) || nowmap() == 0x503) || nowmap() == 0x506) || nowmap() == 0x50c) || nowmap() == 0x50e) || nowmap() == 0x50f) || nowmap() == 0x516) || nowmap() == 0x517) || nowmap() == 0x51c) || nowmap() == 0x51d) || nowmap() == 0x51e))
		{
			bossgaugeinit();
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_short local_var_d;             // pos: 0x0;
	u_char  local_var_e;             // pos: 0x2;
	u_char  local_var_f;             // pos: 0x3;

	function main(1)
	{
		if (nowmap() == 0x483)
		{
			global_var_1 = 0x483;
			if (nowmap() == 0x483)
			{
				questeffectread_1db(0, 47);
			}
			else
			{
				questeffectread_1db(0, 48);
			}
			effectreadsync();
			if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
			{
				musicreadsync(20);
				musicplay(20);
			}
			else
			{
				musicreadsync(91);
				musicplay(91);
			}
			setnowmapmusicno(20);
			startenvsoundall();
			setcharseplayall(1);
			sethpmenufast(0);
			fadein(fadesystemtime());
			fadesync();
			sysReqew(1, trial_select::trial_messages);
			effectplay(0);
			effectsync();
			effectcancel();
			sethpmenu(1);
			resumebattle();
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		if (!(unkCall_5be()))
		{
			if (nowmap() == 0x493)
			{
				if (global_var_1 < 0x493)
				{
					global_var_1 = 0x493;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(35);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(35);
					musicplay(35);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(35);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x4a3)
			{
				if (global_var_1 < 0x4a3)
				{
					global_var_1 = 0x4a3;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(45);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(45);
					musicplay(45);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(45);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x4b3)
			{
				if (global_var_1 < 0x4b3)
				{
					global_var_1 = 0x4b3;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(15);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(15);
					musicplay(15);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(15);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x4c3)
			{
				if (global_var_1 < 0x4c3)
				{
					global_var_1 = 0x4c3;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(85);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(85);
					musicplay(85);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(85);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x4d3)
			{
				if (global_var_1 < 0x4d3)
				{
					global_var_1 = 0x4d3;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(60);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(60);
					musicplay(60);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(60);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x4e3)
			{
				if (global_var_1 < 0x4e3)
				{
					global_var_1 = 0x4e3;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(88);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(88);
					musicplay(88);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(88);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x4f3)
			{
				if (global_var_1 < 0x4f3)
				{
					global_var_1 = 0x4f3;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(48);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(48);
					musicplay(48);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(48);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x503)
			{
				if (global_var_1 < 0x503)
				{
					global_var_1 = 0x503;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(75);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(75);
					musicplay(75);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(75);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
			if (nowmap() == 0x513)
			{
				if (global_var_1 < 0x513)
				{
					global_var_1 = 0x513;
					sethpmenufast(0);
					fadelayer(5);
					fadeout_d0(1, 1);
					fadesync_d3(1);
					fadein(1);
					fadesync();
					amest(0, 0x1000000, 60, 0x411, 1);
					amest(1, 0x1000001, 0x3c0, 0x21c, 4);
					messync2(0);
					messync2(1);
					mkeywaitt(get_pad_ok());
					mkeywaittr(get_pad_ok());
					sebsoundplay(0, 2);
					mesclose(0);
					mesclose(1);
					messync(0, 1);
					wait(30);
					regI0 = nowjumpindex();
					if ((nowmap() == 0x193 && global_flag[0] < 0x541))
					{
						healall(0);
					}
					else if (!((nowmap() == 0x33a && global_flag[0] == 0x17a2)))
					{
						if (!((nowmap() == 187 && global_flag[0] == 0xc30)))
						{
							if (nowmap() >= 0x483)
							{
								healall(5);
							}
							else
							{
								setnowjumpindex(-2);
							}
						}
					}
					openfullscreenmenu_48d(0x8005, 0);
					setnowjumpindex(regI0);
					fadeout(1);
					fadesync();
					fadein_d2(1, 1);
					fadesync_d3(1);
					sethpmenufast(1);
					musicfadeout(-1, 90);
					musicfadeoutsync();
					musicstop(-1);
					wait(1);
					musicclear(-1);
					if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
					{
						musicread(57);
					}
					else
					{
						musicread(91);
					}
					wait(50);
				}
				if (nowmap() == 0x483)
				{
					questeffectread_1db(0, 47);
				}
				else
				{
					questeffectread_1db(0, 48);
				}
				effectreadsync();
				if (!(((getsummonmodebit() || getchocobomodebit()) && !(istownmap()))))
				{
					musicreadsync(57);
					musicplay(57);
				}
				else
				{
					musicreadsync(91);
					musicplay(91);
				}
				setnowmapmusicno(57);
				startenvsoundall();
				setcharseplayall(1);
				sethpmenufast(0);
				fadein(fadesystemtime());
				fadesync();
				effectplay(0);
				effectsync();
				effectcancel();
				sethpmenu(1);
				resumebattle();
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
		}
		unkCall_5bf();
		if ((nowmap() >= 0x483 && nowmap() <= 0x487))
		{
			local_var_f = 1;
			if ((nowmap() > 0x483 && nowmap() <= 0x487))
			{
				local_var_f = (local_var_f + (nowmap() - 0x483));
			}
		}
		if ((nowmap() >= 0x48b && nowmap() <= 0x48f))
		{
			local_var_f = 6;
			if ((nowmap() > 0x48b && nowmap() <= 0x48f))
			{
				local_var_f = (local_var_f + (nowmap() - 0x48b));
			}
		}
		if ((nowmap() >= 0x493 && nowmap() <= 0x497))
		{
			local_var_f = 11;
			if ((nowmap() > 0x493 && nowmap() <= 0x497))
			{
				local_var_f = (local_var_f + (nowmap() - 0x493));
			}
		}
		if ((nowmap() >= 0x49b && nowmap() <= 0x49f))
		{
			local_var_f = 16;
			if ((nowmap() > 0x49b && nowmap() <= 0x49f))
			{
				local_var_f = (local_var_f + (nowmap() - 0x49b));
			}
		}
		if ((nowmap() >= 0x4a3 && nowmap() <= 0x4a7))
		{
			local_var_f = 21;
			if ((nowmap() > 0x4a3 && nowmap() <= 0x4a7))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4a3));
			}
		}
		if ((nowmap() >= 0x4ab && nowmap() <= 0x4af))
		{
			local_var_f = 26;
			if ((nowmap() > 0x4ab && nowmap() <= 0x4af))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4ab));
			}
		}
		if ((nowmap() >= 0x4b3 && nowmap() <= 0x4b7))
		{
			local_var_f = 31;
			if ((nowmap() > 0x4b3 && nowmap() <= 0x4b7))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4b3));
			}
		}
		if ((nowmap() >= 0x4bb && nowmap() <= 0x4bf))
		{
			local_var_f = 36;
			if ((nowmap() > 0x4bb && nowmap() <= 0x4bf))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4bb));
			}
		}
		if ((nowmap() >= 0x4c3 && nowmap() <= 0x4c7))
		{
			local_var_f = 41;
			if ((nowmap() > 0x4c3 && nowmap() <= 0x4c7))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4c3));
			}
		}
		if ((nowmap() >= 0x4cb && nowmap() <= 0x4cf))
		{
			local_var_f = 46;
			if ((nowmap() > 0x4cb && nowmap() <= 0x4cf))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4cb));
			}
		}
		if ((nowmap() >= 0x4d3 && nowmap() <= 0x4d7))
		{
			local_var_f = 51;
			if ((nowmap() > 0x4d3 && nowmap() <= 0x4d7))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4d3));
			}
		}
		if ((nowmap() >= 0x4db && nowmap() <= 0x4df))
		{
			local_var_f = 56;
			if ((nowmap() > 0x4db && nowmap() <= 0x4df))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4db));
			}
		}
		if ((nowmap() >= 0x4e3 && nowmap() <= 0x4e7))
		{
			local_var_f = 61;
			if ((nowmap() > 0x4e3 && nowmap() <= 0x4e7))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4e3));
			}
		}
		if ((nowmap() >= 0x4eb && nowmap() <= 0x4ef))
		{
			local_var_f = 66;
			if ((nowmap() > 0x4eb && nowmap() <= 0x4ef))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4eb));
			}
		}
		if ((nowmap() >= 0x4f3 && nowmap() <= 0x4f7))
		{
			local_var_f = 71;
			if ((nowmap() > 0x4f3 && nowmap() <= 0x4f7))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4f3));
			}
		}
		if ((nowmap() >= 0x4fb && nowmap() <= 0x4ff))
		{
			local_var_f = 76;
			if ((nowmap() > 0x4fb && nowmap() <= 0x4ff))
			{
				local_var_f = (local_var_f + (nowmap() - 0x4fb));
			}
		}
		if ((nowmap() >= 0x503 && nowmap() <= 0x507))
		{
			local_var_f = 81;
			if ((nowmap() > 0x503 && nowmap() <= 0x507))
			{
				local_var_f = (local_var_f + (nowmap() - 0x503));
			}
		}
		if ((nowmap() >= 0x50b && nowmap() <= 0x50f))
		{
			local_var_f = 86;
			if ((nowmap() > 0x50b && nowmap() <= 0x50f))
			{
				local_var_f = (local_var_f + (nowmap() - 0x50b));
			}
		}
		if ((nowmap() >= 0x513 && nowmap() <= 0x517))
		{
			local_var_f = 91;
			if ((nowmap() > 0x513 && nowmap() <= 0x517))
			{
				local_var_f = (local_var_f + (nowmap() - 0x513));
			}
		}
		if ((nowmap() >= 0x51b && nowmap() <= 0x51f))
		{
			local_var_f = 96;
			if ((nowmap() > 0x51b && nowmap() <= 0x51f))
			{
				local_var_f = (local_var_f + (nowmap() - 0x51b));
			}
		}
		if (((((((((nowmap() == 0x49c || nowmap() == 0x4c4) || nowmap() == 0x4d7) || nowmap() == 0x4ed) || nowmap() == 0x503) || nowmap() == 0x506) || nowmap() == 0x513) || nowmap() == 0x517) || nowmap() == 0x51e))
		{
			setshapepos_183(3, 4, 0x64e, 124, 0);
			setshapeshowstatus(3, 4, 1);
			regI0 = ((local_var_f / 100) % 10);
			if (local_var_f > 0x3e7)
			{
				regI0 = 9;
			}
			setshape(3, 1, 1, (0 + regI0));
			setshapepos_183(3, 1, 0x6b5, 126, 0);
			if (regI0 != 0)
			{
				setshapeshowstatus(3, 1, 1);
			}
			else
			{
				setshapeshowstatus(3, 1, 0);
			}
			regI1 = ((local_var_f / 10) % 10);
			if (local_var_f > 0x3e7)
			{
				regI1 = 9;
			}
			setshape(3, 2, 1, (0 + regI1));
			setshapepos_183(3, 2, 0x6cd, 126, 0);
			if ((regI0 != 0 || regI1 != 0))
			{
				setshapeshowstatus(3, 2, 1);
			}
			else
			{
				setshapeshowstatus(3, 2, 0);
			}
			regI2 = (local_var_f % 10);
			if (local_var_f > 0x3e7)
			{
				regI2 = 9;
			}
			setshape(3, 3, 1, (0 + regI2));
			setshapepos_183(3, 3, 0x6e5, 126, 0);
			setshapeshowstatus(3, 3, 1);
		}
		else
		{
			setshapepos_183(3, 4, 0x64e, 58, 0);
			setshapeshowstatus(3, 4, 1);
			regI0 = ((local_var_f / 100) % 10);
			if (local_var_f > 0x3e7)
			{
				regI0 = 9;
			}
			setshape(3, 1, 1, (0 + regI0));
			setshapepos_183(3, 1, 0x6b5, 60, 0);
			if (regI0 != 0)
			{
				setshapeshowstatus(3, 1, 1);
			}
			else
			{
				setshapeshowstatus(3, 1, 0);
			}
			regI1 = ((local_var_f / 10) % 10);
			if (local_var_f > 0x3e7)
			{
				regI1 = 9;
			}
			setshape(3, 2, 1, (0 + regI1));
			setshapepos_183(3, 2, 0x6cd, 60, 0);
			if ((regI0 != 0 || regI1 != 0))
			{
				setshapeshowstatus(3, 2, 1);
			}
			else
			{
				setshapeshowstatus(3, 2, 0);
			}
			regI2 = (local_var_f % 10);
			if (local_var_f > 0x3e7)
			{
				regI2 = 9;
			}
			setshape(3, 3, 1, (0 + regI2));
			setshapepos_183(3, 3, 0x6e5, 60, 0);
			setshapeshowstatus(3, 3, 1);
		}
		if (((((((((((((((((((((((((nowmap() == 0x48f || nowmap() == 0x495) || nowmap() == 0x497) || nowmap() == 0x49b) || nowmap() == 0x49d) || nowmap() == 0x49e) || nowmap() == 0x4a3) || nowmap() == 0x4ae) || nowmap() == 0x4b4) || nowmap() == 0x4b7) || nowmap() == 0x4be) || nowmap() == 0x4c4) || nowmap() == 0x4ce) || nowmap() == 0x4cf) || nowmap() == 0x4ed) || nowmap() == 0x503) || nowmap() == 0x506) || nowmap() == 0x50c) || nowmap() == 0x50e) || nowmap() == 0x50f) || nowmap() == 0x516) || nowmap() == 0x517) || nowmap() == 0x51c) || nowmap() == 0x51d) || nowmap() == 0x51e))
		{
			bossgaugeopen();
		}
		waitv(&scratch1_var_a);
		if (nowmap() != 0x51f)
		{
			ucoff();
			if (!(((((((((((((((((((((((((nowmap() == 0x48f || nowmap() == 0x495) || nowmap() == 0x497) || nowmap() == 0x49b) || nowmap() == 0x49d) || nowmap() == 0x49e) || nowmap() == 0x4a3) || nowmap() == 0x4ae) || nowmap() == 0x4b4) || nowmap() == 0x4b7) || nowmap() == 0x4be) || nowmap() == 0x4c4) || nowmap() == 0x4ce) || nowmap() == 0x4cf) || nowmap() == 0x4ed) || nowmap() == 0x503) || nowmap() == 0x506) || nowmap() == 0x50c) || nowmap() == 0x50e) || nowmap() == 0x50f) || nowmap() == 0x516) || nowmap() == 0x517) || nowmap() == 0x51c) || nowmap() == 0x51d) || nowmap() == 0x51e)))
			{
				suspendbattle_no_resetmotion();
			}
			party_motionsync_and_relax();
			if (((((((((((((((((((((((((nowmap() == 0x48f || nowmap() == 0x495) || nowmap() == 0x497) || nowmap() == 0x49b) || nowmap() == 0x49d) || nowmap() == 0x49e) || nowmap() == 0x4a3) || nowmap() == 0x4ae) || nowmap() == 0x4b4) || nowmap() == 0x4b7) || nowmap() == 0x4be) || nowmap() == 0x4c4) || nowmap() == 0x4ce) || nowmap() == 0x4cf) || nowmap() == 0x4ed) || nowmap() == 0x503) || nowmap() == 0x506) || nowmap() == 0x50c) || nowmap() == 0x50e) || nowmap() == 0x50f) || nowmap() == 0x516) || nowmap() == 0x517) || nowmap() == 0x51c) || nowmap() == 0x51d) || nowmap() == 0x51e))
			{
				bossgaugeclose();
				wait(30);
			}
			battlelastseplaysync(0);
			if (((((((((((((((((((((((((nowmap() == 0x48f || nowmap() == 0x495) || nowmap() == 0x497) || nowmap() == 0x49b) || nowmap() == 0x49d) || nowmap() == 0x49e) || nowmap() == 0x4a3) || nowmap() == 0x4ae) || nowmap() == 0x4b4) || nowmap() == 0x4b7) || nowmap() == 0x4be) || nowmap() == 0x4c4) || nowmap() == 0x4ce) || nowmap() == 0x4cf) || nowmap() == 0x4ed) || nowmap() == 0x503) || nowmap() == 0x506) || nowmap() == 0x50c) || nowmap() == 0x50e) || nowmap() == 0x50f) || nowmap() == 0x516) || nowmap() == 0x517) || nowmap() == 0x51c) || nowmap() == 0x51d) || nowmap() == 0x51e))
			{
				suspendbattle();
			}
			if (((((((((nowmap() == 0x48f || nowmap() == 0x49f) || nowmap() == 0x4af) || nowmap() == 0x4bf) || nowmap() == 0x4cf) || nowmap() == 0x4df) || nowmap() == 0x4ef) || nowmap() == 0x4ff) || nowmap() == 0x50f))
			{
				PC00.resetemergency_one(1);
				PC01.resetemergency_one(1);
				PC02.resetemergency_one(1);
				PC03.resetemergency_one(1);
				musicfadeout(-1, 60);
				stopenvsound_3de(60);
				spotsoundtrans(60, 0);
				questeffectread_1db(0, 43);
				musicfadeoutsync();
				musicstop(-1);
				wait(1);
				musicclear(-1);
				sethpmenu(0);
				musicread(86);
				musicreadsync(86);
				effectreadsync_e1(0);
				musicplay(86);
				effectplay_e2(0, 0);
				wait(30);
				effectplay_e2(0, 1);
				wait(90);
				effectplay_e2(0, 2);
				effectsync_e3(0);
				effectcancel();
				wait(15);
				if (nowmap() == 0x48f)
				{
					sebsoundplay(0, 39);
					additemmes(0x9123, 1);
				}
				if (nowmap() == 0x49f)
				{
					sebsoundplay(0, 39);
					additemmes(0x9124, 1);
				}
				if (nowmap() == 0x4af)
				{
					sebsoundplay(0, 39);
					additemmes(0x9125, 1);
				}
				if (nowmap() == 0x4bf)
				{
					sebsoundplay(0, 39);
					additemmes(0x9126, 1);
				}
				if (nowmap() == 0x4cf)
				{
					sebsoundplay(0, 39);
					additemmes(0x9127, 1);
					unkCall_5ad(39);
				}
				if (nowmap() == 0x4df)
				{
					sebsoundplay(0, 39);
					additemmes(0x9128, 1);
				}
				if (nowmap() == 0x4ef)
				{
					sebsoundplay(0, 39);
					additemmes(0x9129, 1);
				}
				if (nowmap() == 0x4ff)
				{
					sebsoundplay(0, 39);
					additemmes(0x912a, 1);
				}
				if (nowmap() == 0x50f)
				{
					sebsoundplay(0, 39);
					additemmes(0x912b, 1);
				}
				wait(20);
			}
			else
			{
				wait(30);
			}
			fadeout(15);
			fadesync();
			bosskillall();
			zakokillall();
			clear_activelog();
			clearnavimapfootmark();
			if ((((((((nowmap() == 0x49c || nowmap() == 0x4c4) || nowmap() == 0x4d7) || nowmap() == 0x4ed) || nowmap() == 0x503) || nowmap() == 0x506) || nowmap() == 0x513) || nowmap() == 0x51e))
			{
				clear_field_effect(mp_map_flg);
			}
			local_var_d = (nowmap() + 1);
			if (nowmap() == 0x487)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x48f)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x497)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x49f)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4a7)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4af)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4b7)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4bf)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4c7)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4cf)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4d7)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4df)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4e7)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4ef)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4f7)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x4ff)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x507)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x50f)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x517)
			{
				local_var_d = (local_var_d + 3);
			}
			if (nowmap() == 0x483)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x484)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x485)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x486)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x487)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x48b)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x48c)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x48d)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x48e)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x48f)
			{
				local_var_e = 11;
			}
			if (nowmap() == 0x493)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x494)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x495)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x496)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x497)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x49b)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x49c)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x49d)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x49e)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x49f)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x4a3)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4a4)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4a5)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4a6)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4a7)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4ab)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4ac)
			{
				local_var_e = 8;
			}
			if (nowmap() == 0x4ad)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4ae)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4af)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4b3)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x4b4)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x4b5)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x4b6)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4b7)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x4bb)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x4bc)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4bd)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4be)
			{
				local_var_e = 11;
			}
			if (nowmap() == 0x4bf)
			{
				local_var_e = 11;
			}
			if (nowmap() == 0x4c3)
			{
				local_var_e = 11;
			}
			if (nowmap() == 0x4c4)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4c5)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4c6)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4c7)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4cb)
			{
				local_var_e = 6;
			}
			if (nowmap() == 0x4cc)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4cd)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4ce)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4cf)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4d3)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x4d4)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x4d5)
			{
				local_var_e = 19;
			}
			if (nowmap() == 0x4d6)
			{
				local_var_e = 19;
			}
			if (nowmap() == 0x4d7)
			{
				local_var_e = 19;
			}
			if (nowmap() == 0x4db)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4dc)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x4dd)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4de)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4df)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4e3)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4e4)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4e5)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4e6)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4e7)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4eb)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4ec)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4ed)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x4ee)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x4ef)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x4f3)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x4f4)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4f5)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4f6)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4f7)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4fb)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4fc)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4fd)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4fe)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x4ff)
			{
				local_var_e = 2;
			}
			if (nowmap() == 0x503)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x504)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x505)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x506)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x507)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x50b)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x50c)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x50d)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x50e)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x50f)
			{
				local_var_e = 5;
			}
			if (nowmap() == 0x513)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x514)
			{
				local_var_e = 4;
			}
			if (nowmap() == 0x515)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x516)
			{
				local_var_e = 19;
			}
			if (nowmap() == 0x517)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x51b)
			{
				local_var_e = 3;
			}
			if (nowmap() == 0x51c)
			{
				local_var_e = 0;
			}
			if (nowmap() == 0x51d)
			{
				local_var_e = 20;
			}
			if (nowmap() == 0x51e)
			{
				local_var_e = 2;
			}
			fadeout_d0(2, 0);
			fadesync_d3(2);
			fadein(0);
			wait(1);
			mapjump(local_var_d, local_var_e, 1);
		}
		else
		{
			ucoff();
			suspendbattle_no_resetmotion();
			party_motionsync_and_relax();
			battlelastseplaysync(0);
			setnowmapmusicno(-1);
			scratch1_var_b = 1;
			unkCall_5ad(40);
			wait(20);
			musicfadeout_231(getnowmapmusicno(), 60, 1);
			musicfadeout_231(getnowmusicno(), 60, 1);
			stopenvsound_3de(60);
			spotsoundtrans(60, 0);
			fadeout(15);
			fadesync();
			unkCall_5ba(0);
			bosskillall();
			zakokillall();
			clear_activelog();
			clearnavimapfootmark();
			musicplaysync(getnowmusicno());
			unkCall_3083();
			wait(1);
			partyusemapid(1);
			setposparty(128, 3.20000005, 128, 0);
			eventread_e8(0, 0x1ab);
			eventreadsync();
			eventplay_ec(0);
			eventsync_ee(0);
			spotsoundtrans(0, 127);
			startenvsoundall();
			unkCall_5ba(1);
			fadeout_d0(2, 0);
			fadesync_d3(2);
			fadein(0);
			wait(1);
			mapjump(0x47b, 0, 1);
		}
		return;
	}
}


script Map_Director(0)
{

	function init()
	{
		hidemapmodel(11);
		showmapmodel(10);
		bganimeplay_9e(20, 0, 0);
		setnavimapindex(0);
		hidemapmodel(20);
		if (global_var_3 == 0)
		{
			showmapmodel(4);
			hidemapmodel(19);
			bganimeplay_9e(0, 145, 145);
			bganimeplay_9e(6, 0, 0);
			if (global_var_4 == 1)
			{
				showmapmodel(18);
			}
		}
		else
		{
			hidemapmodel(4);
			showmapmodel(19);
			hidemapmodel(18);
			bganimeplay_9e(0, 0x122, 0x122);
			bganimeplay_9e(6, 95, 95);
			bgeffectplay(48);
			bgeffectplay(41);
			bgeffectplay(42);
		}
		if (global_var_4 == 0)
		{
			hidemapmodel(17);
			showmapmodel(3);
			bganimeplay_9e(1, 145, 145);
			bganimeplay_9e(7, 0, 0);
		}
		else
		{
			showmapmodel(17);
			bganimeplay_9e(1, 0x122, 0x122);
			bganimeplay_9e(7, 95, 95);
			bgeffectplay(49);
			if (global_var_3 == 1)
			{
				bgeffectplay(43);
				hidemapmodel(3);
			}
		}
		if (global_var_5 == 0)
		{
			hidemapmodel(13);
			showmapmodel(1);
			bganimeplay_9e(2, 145, 145);
			bganimeplay_9e(8, 0, 0);
		}
		else
		{
			showmapmodel(13);
			hidemapmodel(1);
			bganimeplay_9e(2, 0x122, 0x122);
			bganimeplay_9e(8, 95, 95);
			bgeffectplay(50);
			if (global_var_6 == 1)
			{
				hidemapmodel(1);
				bgeffectplay(45);
			}
		}
		if (global_var_6 == 0)
		{
			hidemapmodel(12);
			if (global_var_5 == 1)
			{
				showmapmodel(14);
			}
			showmapmodel(0);
			bganimeplay_9e(3, 145, 145);
			bganimeplay_9e(9, 0, 0);
		}
		else
		{
			showmapmodel(12);
			hidemapmodel(14);
			hidemapmodel(0);
			bganimeplay_9e(3, 0x122, 0x122);
			bganimeplay_9e(9, 95, 95);
			bgeffectplay(51);
			bgeffectplay(46);
			bgeffectplay(47);
		}
		if (global_var_7 == 0)
		{
			bganimeplay_9e(4, 145, 145);
			hidemapmodel(15);
		}
		else
		{
			if ((global_var_5 == 1 && global_var_6 == 1))
			{
				bgeffectplay(57);
			}
			else
			{
				bgeffectplay(56);
			}
			bganimeplay_9e(4, 0x122, 0x122);
			showmapmodel(15);
		}
		if (global_var_8 == 0)
		{
			bganimeplay_9e(5, 145, 145);
			hidemapmodel(16);
		}
		else
		{
			if ((global_var_3 == 1 && global_var_4 == 1))
			{
				bgeffectplay(53);
			}
			else
			{
				bgeffectplay(52);
			}
			bganimeplay_9e(5, 0x122, 0x122);
			if (((global_var_7 == 1 || global_var_4 == 1) || global_var_5 == 1))
			{
				showmapmodel(16);
				hidemapmodel(2);
			}
			else
			{
				hidemapmodel(16);
			}
		}
		return;
	}
}


script PC00(5) : 0x80
{

	function init()
	{
		setupbattle(0);
		return;
	}
}


script PC01(5) : 0x81
{

	function init()
	{
		setupbattle(1);
		return;
	}
}


script PC02(5) : 0x82
{

	function init()
	{
		setupbattle(2);
		return;
	}
}


script PC03(5) : 0x83
{

	function init()
	{
		setupbattle(3);
		return;
	}
}


script treasure_00(6)
{

	function init()
	{
		setuptreasure(0x3000080);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000080);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000080);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000080);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_01(6)
{

	function init()
	{
		setuptreasure(0x3000098);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000098);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000098);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000098);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}

#include "stage_choice.c"

//======================================================================
//                         File scope variables                         
//======================================================================
char    file_var_9;              // pos: 0xc;


script trial_00(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000038);
		btlAtelSetPoint2(0x3000008);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(0);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(1, 0);
		btlAtelSetEventFlagGroup(1, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(1, 0);
		btlAtelSetEntryFlag(16);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x40000000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_9 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_9 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_9, -1);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					btlAtelSetKeepWorkGroup(0, -1);
					setentrygroup(0);
					requestreentry();
					return;
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}


script trial_01(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x300003a);
		btlAtelSetPoint2(0x3000010);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(1, 0);
		btlAtelSetEventFlagGroup(1, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(1, 0);
		btlAtelSetEntryFlag(16);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x40000000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_9 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_9 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_9, -1);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					btlAtelSetKeepWorkGroup(0, -1);
					setentrygroup(0);
					requestreentry();
					return;
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}


script trial_02(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x300003c);
		btlAtelSetPoint2(0x3000018);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(2);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(1, 0);
		btlAtelSetEventFlagGroup(1, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(1, 0);
		btlAtelSetEntryFlag(16);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x40000000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_9 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_9 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_9, -1);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					btlAtelSetKeepWorkGroup(0, -1);
					setentrygroup(0);
					requestreentry();
					return;
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}


script trial_03(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x300003e);
		btlAtelSetPoint2(0x3000020);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(0);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(1, 0);
		btlAtelSetEventFlagGroup(1, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(16);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x40000000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_9 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_9 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_9, -1);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					btlAtelSetKeepWorkGroup(0, -1);
					setentrygroup(0);
					requestreentry();
					return;
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}


script trial_04(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000040);
		btlAtelSetPoint2(0x3000028);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(1);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(1, 0);
		btlAtelSetEventFlagGroup(1, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(16);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x40000000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_9 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_9 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_9, -1);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					btlAtelSetKeepWorkGroup(0, -1);
					setentrygroup(0);
					requestreentry();
					return;
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}


script trial_05(7)
{

	spawnData(1, false);

	function init()
	{
		btlAtelInitTagStart();
		btlAtelSetTotalEntryNumber(0x3000042);
		btlAtelSetPoint2(0x3000030);
		btlAtelInitTagFinish();
		return;
	}


	function entry00()
	{
		btlAtelSetUnit(2);
		btlAtelSetDirection(-1);
		btlAtelSetDefaultLogic(0);
		btlAtelSetHpLowLimit(0);
		btlAtelSetLevel(1, 0);
		btlAtelSetEventFlagGroup(1, 0);
		btlAtelSetCallGroup(0);
		btlAtelSetUnitType(0);
		btlAtelSetBelong(2, 0);
		btlAtelSetEntryFlag(16);
		btlAtelSetSpecialSpawnRange(-1);
		btlAtelSetDeSpawnType(0, 0, 0);
		btlAtelSetStatus(0x3000000);
		btlAtelSetAbility(0x40000000, 0x4000000);
		btlAtelSetEventData(-1, -1, -1);
		btlAtelSetUnitFinish();
		return;
	}


	function spawn00()
	{
		btlAtelSpawnTagStart();
		if (btlAtelDecideEntryGroupByContinuous() == -1)
		{
			file_var_9 = btlAtelDecideEntryGroupByPreviousMap();
			if (file_var_9 >= 0)
			{
				btlAtelSetKeepWorkGroup(file_var_9, -1);
				setentrygroup(file_var_9);
				requestreentry();
				return;
			}
		}
		while (true)
		{
			if ((btlAtelGetDeadCountNow(0) < btlAtelGetDeadCountMax(0) || btlAtelGetDeadCountMax(0) == 0))
			{
				if ((btlAtelGetElapseTime(0) > btlAtelGetRespawnTime(0) || btlAtelGetDeadCountNow(0) == 0))
				{
					btlAtelSetKeepWorkGroup(0, -1);
					setentrygroup(0);
					requestreentry();
					return;
				}
			}
			wait(1);
		}
	}


	function respawn00()
	{
		btlAtelRespawnTagStart();
		if (btlAtelGetRespawnCause() == 0)
		{
			btlAtelSetBattleLogicFlag(15, 0);
		}
		while (true)
		{
			wait(1);
		}
	}
}

//======================================================================
//                           Map exit arrays                            
//======================================================================

mapExitArray mapExitGroup0[1] = {

	exitStruct mapExit0 = {
		126, -0, 155.302551, 1, 
		133.999954, 0, 155.302551, 0x1409
	};

};

mapExitArray mapExitGroup1[1] = {

	exitStruct mapExit0 = {
		97.25, 4, 126.914574, 1, 
		97.25, 4, 131.248367, 0x10d
	};

};

mapExitArray mapExitGroup2[0] = {

};

mapExitArray mapExitGroup3[0] = {

};

mapExitArray mapExitGroup4[0] = {

};


//======================================================================
//                      Map jump position vectors                       
//======================================================================
mjPosArr1 mapJumpPositions1[3] = {

	mjPos mapJumpPos0 = {
		135.144699, -0, 138.395599, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		101.19529, -0, 128.859283, 1.59237623,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		129.967804, -0, 151.174515, -3.14075971,
		0, 0, 0, 0
	};

};
mjPosArr2 mapJumpPositions2[5] = {

	mjPos mapJumpPos0 = {
		135.144699, -0, 138.395599, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		61.7528458, -0, 128.886627, -1.57467675,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		101.19529, -0, 128.859283, 1.59237623,
		0, 0, 0, 0
	};

	mjPos mapJumpPos3 = {
		130.315247, -0, 163.372208, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos4 = {
		129.967804, -0, 151.174515, -3.14075971,
		0, 0, 0, 0
	};

};


//======================================================================
//                       Unknown position arrays                        
//======================================================================
unkPos1 unknownPosition0[7] = {130.011414, -0, 155.319717, 4.00177383, 3, 0, 0};
unkPos1 unknownPosition1[7] = {130.015228, -0, 154.806152, 2, 1, 0, 0};

unkPos2 unknownPosition0[7] = {130.053864, 1.29999995, 155.333237, 1.5, 1.5, 0, 0};


