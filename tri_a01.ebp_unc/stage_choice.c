#include "sys/pad.h"
#include "sys/message.h"

#define MES_LEVEL_CHOICE (MES_MASK | 2)
#define MES_LEVEL_CONFIRM (MES_MASK | 3)


#define WIN_POS_X 960
#define WIN_POS_Y 540

script trial_select(6)
{

	function init()
	{
		return;
	}

	int trialLevel;
	u_short prevButtons;
	u_short currButtons;
	u_char trialJumpIndexes[100] = {
		0, 3, 3, 0, 4, 4, 0, 5, 4, 4,
		11, 0, 4, 4, 4, 0, 4, 0, 0, 0,
		4, 0, 0, 0, 0, 0, 0, 8, 0, 0,
		0, 3, 3, 3, 0, 4, 3, 0, 0, 11,
		11, 11, 0, 0, 5, 5, 6, 5, 0, 0,
		0, 3, 3, 19, 19, 19, 0, 4, 5, 5,
		0, 0, 0, 0, 0, 0, 0, 0, 4, 4,
		4, 0, 5, 5, 5, 5, 5, 5, 5, 5,
		2, 5, 5, 0, 0, 0, 0, 0, 3, 3,
		5, 0, 4, 0, 19, 0, 3, 0, 20, 2 
	};

	function trial_messages()
	{
		trialLevel = 1;
	restart_label:
		setmesmacro(0, 0, 0, trialLevel);
		mesopenspeed(0, 0);
		ames(0, MES_LEVEL_CHOICE, WIN_POS_X, WIN_POS_Y, WIN_ALIGN_CENTER);

		prevButtons = 0;
		while (true)
		{
			currButtons = pad();
			regI0 = currButtons & (~prevButtons);
			if (regI0 & PAD_LEFT)
			{
				if (trialLevel > 1) trialLevel--;
			}
			else if (regI0 & PAD_RIGHT)
			{
				if (trialLevel < 100) trialLevel++;
			}
			else if (regI0 & PAD_UP)
			{
				if (trialLevel < 90) trialLevel += 10;
				else trialLevel = 100;
			}
			else if (regI0 & PAD_DOWN)
			{
				if (trialLevel > 10) trialLevel -= 10;
				else trialLevel = 1;
			}
			setmesmacro(0, 0, 0, trialLevel);
			updatemesmacro(0, 0);
			prevButtons = currButtons;
			if (currButtons & (get_pad_ok() | get_pad_cancel())) break;
			wait(1);
		}
		mesclose(0);
		messync(0, 1);

		if (currButtons & get_pad_cancel()) return;

		askpos(0, 0, 1);
		setmesmacro(0, 0, 0, trialLevel);
		regI0 = aask(0, MES_LEVEL_CONFIRM, WIN_POS_X, WIN_POS_Y, WIN_ALIGN_CENTER);
		mesclose(0);
		messync(0, 1);

		if (regI0 != 0)
			goto restart_label;
		
		if (trialLevel == 1)
			return;

		pausesestop();
		sebsoundplay(0, 0x5f);
		effectteleposyncbyid(partycommoneffectplay(12));
		whiteout_d1(2, 0);

		mapjump(1154 + ((trialLevel - 1)/5)*3 + trialLevel, trialJumpIndexes[trialLevel-1], 10);

		return;
	}
}
